const GroupChat = require('../models/groupChatModel')

exports.index = async (req, res) => {
  // Declaration
  let userId = req.user.id

  // Try-Catch
  try {
    const groupChat = await GroupChat.find({ participants: userId })
      .populate({
        path: 'participants',
        select: 'username avatar about',
      })
      .populate({
        path: 'createdBy',
        select: 'username avatar about',
      })
    res.status(200).send({
      status: res.statusCode,
      success: true,
      messages: 'Success load data!',
      groupChat,
    })
  } catch (error) {
    console.log(error)
    res.status(500).send({
      status: res.statusCode,
      success: false,
      messages: 'Server error!',
    })
  }
}

exports.show = async (req, res) => {
  // Declaration
  let groupId = req.params.id

  // Try-Catch
  try {
    const groupChat = await GroupChat.findOne({ _id: groupId })
      .populate({
        path: 'participants',
        select: 'username avatar bio',
      })
      .populate({
        path: 'createdBy',
        select: 'username avatar bio',
      })
    res.status(200).send({
      status: res.statusCode,
      success: true,
      messages: 'Success load data!',
      groupChat,
    })
  } catch (error) {
    console.log(error)
    res.status(500).send({
      status: res.statusCode,
      success: false,
      messages: 'Server error!',
    })
  }
}

exports.store = async (req, res) => {
  // Declare
  let groupId = req.body.idGroup
  let userId = req.user._id
  let groupName = req.body.groupName
  let description = req.body.description

  // Try-Catch
  try {
    const createdBy = userId
    const participants = req.body.participants
    const participantsData = participants.concat(createdBy)
    const groupChat = new GroupChat({
      idGroup: groupId,
      createdBy: createdBy,
      participants: participantsData,
      groupName: groupName,
      description: description,
    })
    await groupChat.save()

    // Response
    res.status(201).send({
      status: res.statusCode,
      success: true,
      messages: 'New group created!',
    })
  } catch (error) {
    console.log(error)

    // Response
    res.status(500).send({
      status: res.statusCode,
      success: false,
      messages: 'Server error!',
    })
  }
}

exports.update = async (req, res) => {
  // Declaration
  let groupId = req.params.id

  try {
    await GroupChat.findOneAndUpdate(groupId, { $set: req.body })
    res.send({
      status: res.statusCode,
      success: true,
      messages: 'Data updated!',
    })
  } catch (error) {
    console.log(error)
    res.send({
      status: res.statusCode,
      success: false,
      messages: 'Server error!',
    })
  }
}

exports.destroy = async (req, res) => {
  // Declaration
  let groupId = req.params.id

  try {
    await GroupChat.findOneAndDelete(groupId)
    res.send({
      status: res.statusCode,
      success: true,
      messages: 'Data deleted!',
    })
  } catch (error) {
    console.log(error)
    res.send({
      status: res.statusCode,
      success: false,
      messages: 'Server error!',
    })
  }
}

exports.storeParticipants = async (req, res) => {
  // Declaration
  let groupId = req.params.id

  // Try-Catch
  try {
    await GroupChat.findOneAndUpdate(groupId, {
      $push: { participants: { $each: req.body.participants } },
    })
    res.status(200).send({
      status: res.statusCode,
      success: true,
      messages: 'New Participants added',
    })
  } catch (error) {
    console.log(error)
    res.status(500).send({
      status: res.statusCode,
      success: false,
      messages: 'Server error!',
    })
  }
}

exports.destroyParticipants = async (req, res) => {
  // Declaration
  let participants = req.body.participants

  // Try-Catch
  try {
    const user = await GroupChat.findOneAndUpdate(
      req.params.id,
      { $pull: { participants: participants } },
      { safe: true }
    )
    res.status(200).send({
      status: res.statusCode,
      success: true,
      messages: 'Contact deleted!',
    })
  } catch (error) {
    console.log(error)
    res.status(500).send({
      status: res.statusCode,
      success: false,
      messages: 'Server error!',
    })
  }
}
