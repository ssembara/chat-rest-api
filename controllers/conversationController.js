const Conversation = require('../models/conversationModel')
const User = require('../models/userModel')

exports.index = async (req, res) => {
  try {
    const conversation = await Conversation.find({
      participants: req.user._id,
    }).populate({
      path: 'participants',
      select: 'username avatar about',
    })

    res.status(200).send({
      status: res.statusCode,
      success: true,
      messages: 'Success load data!',
      conversation,
    })
  } catch (e) {
    res.status(500).send({
      status: res.statusCode,
      success: false,
      messages: 'Server error!',
    })
    console.log(e)
  }
}

exports.store = async (req, res) => {
  // Declaration
  let id_a = req.user._id
  let id_b = req.body.id

  // Try-Catch
  try {
    const conversationData = await Conversation.findOne({
      participants: [id_a, id_b],
    })

    if (conversationData) {
      res.status(200).send({
        status: res.statusCode,
        success: true,
        messages: 'Conversation was available!',
      })
    } else {
      const conversation = new Conversation({
        participants: [id_a, id_b],
      })
      await conversation.save()
      res.status(201).send({
        status: res.statusCode,
        success: true,
        messages: 'New conversation created!',
      })
    }
  } catch (error) {
    res.status(400).send({
      status: res.statusCode,
      success: false,
      messages: 'Failed to make conversation!',
    })
    console.log(error)
  }
}

exports.show = async (req, res) => {
  try {
    const conversation = await Conversation.findById({
      _id: req.params.id,
    }).populate({
      path: 'participants',
      select: 'username avatar about',
    })
    res.status(200).send({
      status: res.statusCode,
      success: true,
      messages: 'Success load data!',
      conversation,
    })
  } catch (e) {
    res.status(500).send({
      status: res.statusCode,
      success: false,
      messages: 'Server error!',
    })
    console.log(e)
  }
}

exports.destroy = async (req, res) => {
  // Try-Catch
  try {
    await Conversation.findByIdAndDelete({ _id: req.params.id })
    res.status(200).send({
      status: res.statusCode,
      success: true,
      messages: 'Success delete data!',
    })
  } catch (error) {
    res.status(500).send({
      status: res.statusCode,
      success: false,
      messages: 'Server error!',
    })
    console.log(error)
  }
}
