// Dependency
const mongoose = require('mongoose')
const timeZone = require('mongoose-timezone')
const uniqueValidator = require('mongoose-unique-validator')
const validator = require('validator')

// Get the Schema constructor
const Schema = mongoose.Schema

// Using Schema constructor, create a contactSchema
const contactSchema = new Schema(
  {
    listContacts: [
      {
        type: Schema.Types.ObjectId,
        ref: 'User',
      },
    ],
    ownerId: {
      type: Schema.Types.ObjectId,
      required: true,
      unique: true,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
)

// schemaPlugin
contactSchema.plugin(timeZone, { paths: ['timestamps'] }, uniqueValidator)

const Contact = mongoose.model('Contact', contactSchema)
module.exports = Contact
