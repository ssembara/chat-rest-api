const authUser = require('../middleware/authUser')
const groupChatController = require('../controllers/groupChatController')
const express = require('express')
const app = express.Router()

// Manage groupChat
app.get('/', authUser, groupChatController.index)
app.get('/:id', authUser, groupChatController.show)
app.post('/store', authUser, groupChatController.store)
app.put('/:id/update', authUser, groupChatController.update)
app.delete('/:id/destroy', authUser, groupChatController.destroy)

// Manage participants in groupChat
app.put('/:id/addParticipants', authUser, groupChatController.storeParticipants)
app.delete(
  '/:id/destroyParticipants',
  authUser,
  groupChatController.destroyParticipants
)

module.exports = app
